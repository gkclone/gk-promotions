<?php
/**
 * @file
 * gk_promotions_domain.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function gk_promotions_domain_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'domain_promotion'
  $field_bases['domain_promotion'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'promotion',
    ),
    'field_name' => 'domain_promotion',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  return $field_bases;
}
