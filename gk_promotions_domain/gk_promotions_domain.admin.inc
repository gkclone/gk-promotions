<?php

/**
 * Settings form.
 */
function gk_promotions_domain_admin_settings_form($form, &$form_state) {
  $form['gk_promotions_domain'] = array(
    '#type' => 'fieldset',
    '#title' => 'Domain content assignment',
  );

  // Options for configuring content behavior.
  $form['gk_promotions_domain']['gk_promotions_domain_content_assignment_behavior'] = array(
    '#type' => 'select',
    '#title' => 'Behavior',
    '#description' => t('Note that in order for changes to this field to take affect you must also submit the form !here.', array(
      '!here' => l('here', 'admin/structure/domain/entities'),
    )),
    '#options' => array(
      DOMAIN_ENTITY_BEHAVIOR_AUTO => 'Auto',
      DOMAIN_ENTITY_BEHAVIOR_USER => 'User',
    ),
    '#default_value' => variable_get('gk_promotions_domain_content_assignment_behavior', DOMAIN_ENTITY_BEHAVIOR_USER),
  );

  $form['gk_promotions_domain']['gk_promotions_domain_content_assignment_default'] = array(
    '#type' => 'select',
    '#title' => 'Default',
    '#options' => array(
      DOMAIN_ALL => 'Send to all affiliates',
      DOMAIN_ACTIVE => 'Current domain',
    ),
    '#default_value' => variable_get('gk_promotions_domain_content_assignment_default', DOMAIN_ALL),
  );

  return system_settings_form($form);
}
