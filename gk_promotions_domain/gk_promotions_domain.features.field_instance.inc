<?php
/**
 * @file
 * gk_promotions_domain.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function gk_promotions_domain_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'promotion-image-domain_promotion'
  $field_instances['promotion-image-domain_promotion'] = array(
    'bundle' => 'image',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'promotion',
    'field_name' => 'domain_promotion',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'promotion-text-domain_promotion'
  $field_instances['promotion-text-domain_promotion'] = array(
    'bundle' => 'text',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'promotion',
    'field_name' => 'domain_promotion',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'promotion-text_image-domain_promotion'
  $field_instances['promotion-text_image-domain_promotion'] = array(
    'bundle' => 'text_image',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'banner' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'promotion',
    'field_name' => 'domain_promotion',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Domain');

  return $field_instances;
}
