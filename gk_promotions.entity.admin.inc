<?php

/**
 * @file
 * Admin UI for the promotion(_type) entities.
 */

/**
 * Entity: promotion ================================================================
 */

/**
 * Entity form: promotion
 */
function promotion_form($form, &$form_state, $promotion) {
  $form_state['promotion'] = $promotion;
  $entity_id = entity_id('promotion', $promotion);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
    '#required' => TRUE,
    '#default_value' => $promotion->title,
    '#weight' => -10,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $promotion->uid,
  );

  // Field API form elements.
  field_attach_form('promotion', $promotion, $form, $form_state);

  // Display options.
  $form['display_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Display options',
    '#group' => 'additional_settings',
  );

  $form['display_options']['url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL',
    '#default_value' => $promotion->url,
  );

  $form['display_options']['cta'] = array(
    '#type' => 'textfield',
    '#title' => 'CTA',
    '#attributes' => array(
      'placeholder' => 'e.g. Sign up',
    ),
    '#default_value' => $promotion->cta,
  );

  // Visibility options.
  $form['visibility_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Visibility options',
    '#group' => 'additional_settings',
  );

  $form['visibility_options']['days'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Days',
    '#description' => 'Only show the promotion on the selected days',
    '#options' => gk_promotions_days_of_the_week(),
    '#default_value' => !empty($promotion->days) ? array_keys($promotion->days) : array(),
  );

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $promotion->status,
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate handler for the promotion entity form.
 */
function promotion_form_validate($form, &$form_state) {
  // Validate the 'url' property.
  if (!empty($form_state['values']['url']) && !link_validate_url(trim($form_state['values']['url']))) {
    form_set_error('url', 'The value provided is not a valid URL.');
  }
}

/**
 * Submit handler for the promotion entity form.
 */
function promotion_form_submit($form, &$form_state) {
  $promotion = $form_state['promotion'];

  $form_state['values']['days'] = implode(',', array_filter($form_state['values']['days']));
  $form_state['values']['url'] = link_cleanup_url($form_state['values']['url']);

  if (link_validate_url($form_state['values']['url']) == LINK_INTERNAL && $path = drupal_lookup_path('source', $form_state['values']['url'])) {
    $form_state['values']['url'] = $path;
  }

  entity_form_submit_build_entity('promotion', $promotion, $form, $form_state);
  entity_save('promotion', $promotion);

  $uri = entity_uri('promotion', $promotion);
  $form_state['redirect'] = $uri['path'];

  drupal_set_message(t('Promotion %title saved.', array(
    '%title' => entity_label('promotion', $promotion),
  )));
}

/**
 * Entity: promotion_type ===========================================================
 */

/**
 * Entity form: promotion_type
 */
function promotion_type_form($form, &$form_state, $promotion_type, $op = 'edit') {
  if ($op == 'clone') {
    $promotion_type->label .= ' (cloned)';
    $promotion_type->type = '';
  }

  $form_state['promotion_type'] = $promotion_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($promotion_type->label) ? $promotion_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($promotion_type->type) ? $promotion_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $promotion_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_promotions_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($promotion_type->description) ? $promotion_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the promotion_type entity form.
 */
function promotion_type_form_submit(&$form, &$form_state) {
  $promotion_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('promotion_type', $promotion_type);

  // Redirect the user back to the list of promotion_type entities.
  $form_state['redirect'] = 'admin/structure/promotion-types';
}
