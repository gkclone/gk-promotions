<?php
/**
 * @file
 * gk_promotions.features.inc
 */

/**
 * Implements hook_default_promotion_type().
 */
function gk_promotions_default_promotion_type() {
  $items = array();
  $items['image'] = entity_import('promotion_type', '{ "type" : "image", "label" : "Image", "description" : "" }');
  $items['text'] = entity_import('promotion_type', '{ "type" : "text", "label" : "Text", "description" : "" }');
  $items['text_image'] = entity_import('promotion_type', '{ "type" : "text_image", "label" : "Text and Image", "description" : "" }');
  return $items;
}
