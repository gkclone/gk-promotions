<?php

/**
 * @file
 * Entity controller classes for the promotion(_type) entities.
 */

/**
 * Entity controller class: promotion
 */
class PromotionController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'cta' => '',
      'url' => '',
      'days' => '',
      'status' => 1,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  function attachLoad(&$queried_promotions, $revision_id = FALSE) {
    foreach ($queried_promotions as &$promotion) {
      $days = array();

      if (is_string($promotion->days)) {
        $days = array_intersect_key(gk_promotions_days_of_the_week(), array_flip(explode(',', $promotion->days)));
      }

      $promotion->days = $days;
    }

    parent::attachLoad($queried_promotions, $revision_id);
  }
}
