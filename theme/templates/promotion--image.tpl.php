<div<?php print $attributes; ?>>
  <?php if ($is_link): ?><a<?php print $link_attributes; ?>><?php endif; ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php print $image; ?>
    <?php if ($cta): ?>
      <div class="Promotion-cta Button"><?php print $cta; ?></div>
    <?php endif; ?>
  <?php if ($is_link): ?></a><?php endif; ?>
</div>
