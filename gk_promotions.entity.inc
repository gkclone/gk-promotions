<?php

/**
 * @file
 * Entity classes for the promotion(_type) entities.
 */

/**
 * Entity class: promotion
 */
class Promotion extends Entity {
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/promotion/' . $this->identifier(),
    );
  }
}
