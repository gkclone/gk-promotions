<?php

/**
 * @file
 * API functions for the promotion(_type) entities.
 */

/**
 * Entity: promotion ================================================================
 */

/**
 * Entity access callback: promotion
 */
function promotion_access($op, $promotion, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  static $administer_promotions = array(
    'create' => 1, 'update' => 1, 'edit' => 1, 'delete' => 1,
  );
  if (isset($administer_promotions[$op])) {
    return user_access('administer promotion', $account);
  }

  if ($op == 'view') {
    // Respect the 'days' property by making this promotion accessible only on
    // the days that have been specified, if any.
    if (!empty($promotion->days) && !isset($promotion->days[date('N')])) {
      return FALSE;
    }

    // If we get here then apply a generic content access rule.
    return $promotion->status && user_access('access content', $account);
  }
}

/**
 * Entity view callback: promotion
 */
function promotion_view($promotion, $view_mode = 'default', $langcode = NULL, $page = NULL) {
  return entity_view('promotion', array($promotion), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: promotion
 */
function promotion_load($tid, $reset = FALSE) {
  $promotions = promotion_load_multiple(array($tid), array(), $reset);
  return reset($promotions);
}

/**
 * Entity load multiple callback: promotion
 */
function promotion_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('promotion', $tids, $conditions, $reset);
}

/**
 * Entity: promotion_type ===========================================================
 */

/**
 * List of promotion_type entities.
 */
function gk_promotions_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('promotion_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): promotion_type
 */
function promotion_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer promotion types', $account);
}

/**
 * Entity load callback: promotion_type
 */
function promotion_type_load($promotion_type) {
  return gk_promotions_type_list($promotion_type);
}
