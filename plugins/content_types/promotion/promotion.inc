<?php

$plugin = array(
  'title' => t('Promotion'),
);

function gk_promotions_promotion_content_type_content_types() {
  return gk_promotions_promotion_content_type_get_info();
}

function gk_promotions_promotion_content_type_content_type($subtype) {
  return gk_promotions_promotion_content_type_get_info($subtype);
}

function gk_promotions_promotion_content_type_render($subtype, $conf) {
  if ($promotion = promotion_load($subtype)) {
    $content = array();

    if (promotion_access('view', $promotion)) {
      $content = promotion_view($promotion, $conf['view_mode']);
    }

    return (object) array(
      'content' => $content,
    );
  }
}

function gk_promotions_promotion_content_type_edit_form($form, &$form_state) {
  $entity_info = entity_get_info('promotion');
  $view_modes = array('default' => t('Default'));

  foreach ($entity_info['view modes'] as $key => $item) {
    $view_modes[$key] = $item['label'];
  }

  $form['view_mode'] = array(
    '#type' => 'select',
    '#title' => t('View Mode'),
    '#options' => $view_modes,
    '#default_value' => isset($form_state['conf']['view_mode']) ? $form_state['conf']['view_mode'] : NULL,
  );

  return $form;
}

function gk_promotions_promotion_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}

function gk_promotions_promotion_content_type_admin_title($subtype, $conf) {
  $promotion = gk_promotions_promotion_content_type_get_info($subtype);

  if (empty($promotion)) {
    return t('Deleted/missing promotion @subtype_id', array(
      '@subtype_id' => $subtype,
    ));
  }

  return t('Promotion: @title', array(
    '@title' => $promotion['title'],
  ));
}

function gk_promotions_promotion_content_type_admin_info($subtype, $conf) {
  $promotion_info = gk_promotions_promotion_content_type_get_info($subtype);

  if (!empty($promotion_info)) {
    return (object) array(
      'title' => $promotion_info['title'],
      'content' => '',
    );
  }
}

/**
 * Get info about all promotions, or a specific one if $pid != NULL.
 */
function gk_promotions_promotion_content_type_get_info($pid = NULL) {
  $promotions = array();

  $query = db_select('promotion', 'p')
    ->fields('p', array('pid', 'type', 'title', 'status'))
    ->orderBy('p.title');

  if (!empty($pid)) {
    $query->condition('pid', $pid);
  }

  $result = $query->execute()->fetchAllAssoc('pid');

  foreach ($result as $promotion) {
    $status = $promotion->status ? '' : ' (unpublished)';

    $promotions[$promotion->pid] = array(
      'title' => $promotion->title . $status,
      'icon' => 'icon_' . $promotion->type . '.png',
      'category' => t('Promotions'),
    );
  }

  if (isset($promotions[$pid])) {
    return $promotions[$pid];
  }

  return $promotions;
}
