(function($) {

Drupal.behaviors.gk_promotion_groups_slideshow_thumbnails = {
  attach: function (context, settings) {
    var slideshow = $('.PromotionGroup--formatSlideshowThumbnails');
    var progress = $('.PromotionGroup-progress .inner', slideshow);
    var thumbnails = '';

    //Register slideshow event handlers.
    slideshow.on({
      'cycle-bootstrap' : function( e, opts ) {
        for (var i = 0; i < Drupal.settings.thumbnail_images.length; i++) {
          thumbnails += Drupal.settings.thumbnail_images[i];
        }

        thumbnails = $('<div class="Grid Grid--space"></div>').append(thumbnails);
        slideshow.find('.PromotionGroup-pager').append(thumbnails);
      },

      'cycle-initialized cycle-before' : function( e, opts ) {
        progress.stop(true).css( 'width', 0 );

      },

      'cycle-initialized cycle-after' :  function(e, opts) {
        if ( ! slideshow.is('.cycle-paused') ) {
          progress.animate({ width: '100%' }, opts.timeout, 'linear' );
        }
      },

      'cycle-paused' : function(e, opts, timeoutRemaining) {
        progress.stop(true);
      },

      'cycle-resumed' : function(e, opts, timeoutRemaining) {
        progress.animate({ width: '100%' }, timeoutRemaining, 'linear' );
      }
    });

    //Init slideshow
    slideshow.cycle();

    // Add Google Analytics events so we can track interaction with the banner.
    if (typeof _gaq !== 'undefined') {
      slideshow.find('.PromotionGroup-prev').click(function() {
        _gaq.push(['_trackEvent', 'Promotion group', 'Previous']);
      });

      slideshow.find('.PromotionGroup-next').click(function() {
        _gaq.push(['_trackEvent', 'Promotion group', 'Next']);
      });

      slideshow.find('.PromotionGroup-pager span').each(function(index) {
        $(this).click(function() {
          _gaq.push(['_trackEvent', 'Promotion group', 'Pager', '', index]);
        });
      });

      slideshow.find('.Promotion a').click(function(e) {
        var $this = $(this);
        e.preventDefault();
        _gaq.push(
          ['_trackEvent', 'Promotion group', 'Click', $this.data('promotion-title'), $this.data('promotion-pid')],
          function() {
            window.location = $this.attr('href');
          }
        );
      });
    }
  }
}

})(jQuery);
