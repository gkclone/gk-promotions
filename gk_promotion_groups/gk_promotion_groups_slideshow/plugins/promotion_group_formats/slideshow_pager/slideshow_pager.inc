<?php

/**
 * Preprocess variables for the entity template.
 */
function gk_promotion_groups_format_slideshow_pager_preprocess(&$variables) {
  $variables['title_attributes_array']['class'][] = 'is-hiddenVisually';

  $variables['attributes_array'] += array(
    'data-cycle-fx' => 'fade',
    'data-cycle-timeout' => 5000,
    'data-cycle-speed' => 600,
    'data-cycle-slides' => '.Promotion',
    'data-cycle-pager' => '> .PromotionGroup-controls > .PromotionGroup-pager',
    'data-cycle-prev' => '> .PromotionGroup-controls > .PromotionGroup-prev',
    'data-cycle-next' => '> .PromotionGroup-controls > .PromotionGroup-next',
    'data-cycle-pager-template' => '',
    'data-cycle-pause-on-hover' => 'true',
    'data-cycle-swipe' => 'true',
  );

  $variables['show_controls'] = count($variables['promotions']) > 1;

  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js', array('scope' => 'footer'));
  drupal_add_js(libraries_get_path('cycle2.swipe') . '/jquery.cycle2.swipe.min.js', array('scope' => 'footer'));

  $format_info = $variables['format_info'];
  $module_path = drupal_get_path('module', $format_info['module']);
  $js_path = $module_path . '/' . $format_info['path'] . '/slideshow_pager.js';

  drupal_add_js($js_path, array('scope' => 'footer'));
}
