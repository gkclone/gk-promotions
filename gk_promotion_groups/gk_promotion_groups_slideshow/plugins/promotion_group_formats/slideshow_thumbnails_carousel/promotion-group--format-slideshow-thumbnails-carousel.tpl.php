<div<?php print $attributes; ?>>
  <div class="PromotionGroup-slides">
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <div class="PromotionGroup-progress"><div class="inner"></div></div>

    <?php if ($show_controls): ?>
    <?php endif; ?>

    <?php foreach ($promotions as $promotion): ?>
      <?php print render($promotion); ?>
    <?php endforeach; ?>

  </div>
  <div class="PromotionGroup-pager">
    <div class="PromotionGroup-thumbnails"></div>
    <div class="PromotionGroup-controls">
      <div class="PromotionGroup-prev Icon--arrowLeft"></div>
      <div class="PromotionGroup-next Icon--arrowRight"></div>
    </div>
  </div>
</div>
