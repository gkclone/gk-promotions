<?php

/**
 * Preprocess variables for the entity template.
 */
function gk_promotion_groups_format_slideshow_thumbnails_carousel_preprocess(&$variables) {
 $variables['title_attributes_array']['class'][] = 'is-hiddenVisually';

  // Compile and render thumbnail images, ready to be passed to js
  $thumbnail_images = array();

  foreach ($variables['promotions'] as $promotion) {
    $render = reset($promotion['promotion']);
    $promotion = $render['#entity'];

    if ($image = field_get_items('promotion', $promotion, 'field_promotion_image')) {
      $image = theme('image_style', array(
        'style_name' => 'gk_thumbnail_large',
        'path' => $image[0]['uri'],
      ));

      $thumbnail_image = array(
        '#theme' => 'link',
        '#text' => $image,
        '#path' => '#',
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(),
         ),
      );

      $thumbnail_images[] = drupal_render($thumbnail_image);
    }
  }


  // Setup jQuery Cycle plugin for the main slideshow
  $variables['attributes_array'] += array(
    'data-cycle-fx' => 'fade',
    'data-cycle-paused' => 'true',
    'data-cycle-slides' => '.Promotion',
    'data-cycle-swipe' => 'true',
  );

  // Setup the carousel attributes, to be passed to js.
  $carousel_attributes_array = array(
    'data-cycle-fx' => 'carousel',
    'data-cycle-carousel-visible' => 4,
    'data-cycle-pause-on-hover' => 'true',
    'data-cycle-carousel-fluid' => 'true',
    'data-allow-wrap' => 'true',
    'data-cycle-slides' => '> a',
    'data-cycle-prev' => '+ .PromotionGroup-controls > .PromotionGroup-prev',
    'data-cycle-next' => '+ .PromotionGroup-controls > .PromotionGroup-next',
  );

  // Pass thumbnails and carousel attributes array to JS
  drupal_add_js(array(
    'thumbnail_images' => $thumbnail_images,
    'carousel_attributes_array' => $carousel_attributes_array
    ), 'setting');

  $variables['show_controls'] = count($variables['promotions']) > 1;

  // Add all cycle2 js files
  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js', array('scope' => 'footer'));
  drupal_add_js(libraries_get_path('cycle2.swipe') . '/jquery.cycle2.swipe.min.js', array('scope' => 'footer'));
  drupal_add_js(libraries_get_path('cycle2.carousel') . '/jquery.cycle2.carousel.min.js', array('scope' => 'footer'));

  $format_info = $variables['format_info'];
  $module_path = drupal_get_path('module', $format_info['module']);
  $js_path = $module_path . '/' . $format_info['path'] . '/slideshow_thumbnails_carousel.js';

  drupal_add_js($js_path, array('scope' => 'footer'));
}
