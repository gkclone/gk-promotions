(function($) {

Drupal.behaviors.gk_promotion_groups_slideshow_thumbnails_carousel = {
  attach: function (context, settings) {
    var slideshow = $('.PromotionGroup--formatSlideshowThumbnailsCarousel');

    // Carousel creation and setup
    var carousel = $('.PromotionGroup--formatSlideshowThumbnailsCarousel .PromotionGroup-pager .PromotionGroup-thumbnails');

    var progress = $('.PromotionGroup-progress .inner', slideshow);
    var thumbnails = '';

    // Apply carousel settings
    carousel.attr(Drupal.settings.carousel_attributes_array);

    // Register carousel event handlers
    carousel.on({
      'cycle-before' : function( e, opts ) {
        // When the carousel changes, progress the slideshow accordingly
        slideshow.cycle( 'goto', opts.slideNum -1 );
      },
    })
    // Progress both slideshows on carousel click
    .on('click', '.cycle-slide', function(event) {
      event.preventDefault();
      // slideshow.cycle( 'goto', $(this).data('cycle-index') );
      carousel.cycle( 'goto', $(this).data('cycle-index') );
    });

    // Register slideshow event handlers.
    slideshow.on({
      'cycle-bootstrap' : function( e, opts ) {
        /*
         * Although the cycle-bootstrap event is bound to the slideshow, not the
         * carousel, it seems to run for both. This fixes that.
         */
        if (opts.cycleFx != 'carousel') {
          for (var i = 0; i < Drupal.settings.thumbnail_images.length; i++) {
            var $thumbnail = $(decodeURIComponent(Drupal.settings.thumbnail_images[i]));
            $thumbnail.attr('data-cycle-index', i);
            thumbnails += $thumbnail[0].outerHTML;
          }
          // Add thumbnail images to the carousel container
          carousel.append(thumbnails);
          // Init carousel
          carousel.cycle();
        }
      },

      'cycle-initialized cycle-before' : function( e, opts ) {
        progress.stop(true).css( 'width', 0 );
      },

      'cycle-initialized cycle-after' :  function(e, opts) {
        if ( ! slideshow.is('.cycle-paused') ) {
          progress.animate({ width: '100%' }, opts.timeout, 'linear' );
        }
      },

      'cycle-paused' : function(e, opts, timeoutRemaining) {
        progress.stop(true);
      },

      'cycle-resumed' : function(e, opts, timeoutRemaining) {
        progress.animate({ width: '100%' }, timeoutRemaining, 'linear' );
      },

      'cycle-next cycle-prev' : function( e, opts ) {
        carousel.cycle( 'goto', opts.currSlide );
      },
    });

    //Init slideshow
    slideshow.cycle();

    // Add Google Analytics events so we can track interaction with the banner.
    if (typeof _gaq !== 'undefined') {
      slideshow.find('.PromotionGroup-prev').click(function() {
        _gaq.push(['_trackEvent', 'Promotion group', 'Previous']);
      });

      slideshow.find('.PromotionGroup-next').click(function() {
        _gaq.push(['_trackEvent', 'Promotion group', 'Next']);
      });

      slideshow.find('.PromotionGroup-pager span').each(function(index) {
        $(this).click(function() {
          _gaq.push(['_trackEvent', 'Promotion group', 'Pager', '', index]);
        });
      });

      slideshow.find('.Promotion a').click(function(e) {
        var $this = $(this);
        e.preventDefault();
        _gaq.push(
          ['_trackEvent', 'Promotion group', 'Click', $this.data('promotion-title'), $this.data('promotion-pid')],
          function() {
            window.location = $this.attr('href');
          }
        );
      });
    }
  }
}

})(jQuery);
