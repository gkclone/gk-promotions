<?php

$plugin = array(
  'title' => t('Promotion group'),
);

function gk_promotion_groups_promotion_group_content_type_content_types() {
  return gk_promotion_groups_promotion_group_content_type_get_info();
}

function gk_promotion_groups_promotion_group_content_type_content_type($subtype) {
  return gk_promotion_groups_promotion_group_content_type_get_info($subtype);
}

function gk_promotion_groups_promotion_group_content_type_render($subtype, $conf) {
  if ($promotion_group = promotion_group_load($subtype)) {
    $content = array();

    if (promotion_group_access('view', $promotion_group)) {
      $content = promotion_group_view($promotion_group);
    }

    return (object) array(
      'content' => $content,
    );
  }
}

function gk_promotion_groups_promotion_group_content_type_admin_title($subtype, $conf) {
  $promotion_group = gk_promotion_groups_promotion_group_content_type_get_info($subtype);

  if (empty($promotion_group)) {
    return t('Deleted/missing promotion group @subtype_id', array(
      '@subtype_id' => $subtype,
    ));
  }

  return t('Promotion group: @title', array(
    '@title' => $promotion_group['title'],
  ));
}

function gk_promotion_groups_promotion_group_content_type_admin_info($subtype, $conf) {
  $promotion_group = gk_promotion_groups_promotion_group_content_type_get_info($subtype);

  if (!empty($promotion_group)) {
    return (object) array(
      'title' => $promotion_group['title'],
      'content' => '',
    );
  }
}

/**
 * Get info about all promotion groups, or a specific one if $pgid != NULL.
 */
function gk_promotion_groups_promotion_group_content_type_get_info($pgid = NULL) {
  $promotion_groups = array();

  $query = db_select('promotion_group', 'pg')
    ->fields('pg', array('pgid', 'type', 'title', 'status'))
    ->orderBy('pg.title');

  if (!empty($pgid)) {
    $query->condition('pgid', $pgid);
  }

  $result = $query->execute()->fetchAllAssoc('pgid');

  foreach ($result as $promotion_group) {
    $status = $promotion_group->status ? '' : ' (unpublished)';

    $promotion_groups[$promotion_group->pgid] = array(
      'title' => $promotion_group->title . $status,
      'icon' => 'icon_' . $promotion_group->type . '.png',
      'category' => t('Promotion groups'),
    );
  }

  if (isset($promotion_groups[$pgid])) {
    return $promotion_groups[$pgid];
  }

  return $promotion_groups;
}
