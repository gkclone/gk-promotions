<?php
/**
 * @file
 * gk_promotion_groups.features.inc
 */

/**
 * Implements hook_default_promotion_group_type().
 */
function gk_promotion_groups_default_promotion_group_type() {
  $items = array();
  $items['promotion_group_type__default'] = entity_import('promotion_group_type', '{
    "type" : "promotion_group_type__default",
    "label" : "Default",
    "description" : ""
  }');
  return $items;
}
