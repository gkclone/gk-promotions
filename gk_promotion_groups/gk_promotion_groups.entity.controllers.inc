<?php

/**
 * @file
 * Entity controller classes for the promotion_group(_type) entities.
 */

/**
 * Entity controller class: promotion_group
 */
class PromotionGroupController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'status' => 1,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'default', $langcode = NULL, $content = array()) {
    // Find this groups promotions and attach them to the output.
    if ($promotions_field = field_get_items('promotion_group', $entity, 'field_promotion_group_promotions')) {
      $tids = array();

      foreach ($promotions_field as $field) {
        $tids[] = $field['target_id'];
      }

      if ($promotions = promotion_load_multiple($tids)) {
        foreach ($promotions as $promotion) {
          $promotion_build = promotion_view($promotion);
          $content['promotions'][$promotion->tid] = reset($promotion_build['promotion']);
        }
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
