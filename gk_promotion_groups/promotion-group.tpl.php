<div<?php print $attributes; ?>>
  <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php foreach ($promotions as $promotion): ?>
    <?php print render($promotion); ?>
  <?php endforeach; ?>
</div>
