<?php

/**
 * @file
 * Admin UI for the promotion_group(_type) entities.
 */

/**
 * Entity: promotion_group =====================================================
 */

/**
 * Entity form: promotion_group
 */
function promotion_group_form($form, &$form_state, $promotion_group) {
  $form_state['promotion_group'] = $promotion_group;
  $entity_id = entity_id('promotion_group', $promotion_group);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
    '#required' => TRUE,
    '#default_value' => $promotion_group->title,
    '#weight' => -10,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $promotion_group->uid,
  );

  // Field API form elements.
  field_attach_form('promotion_group', $promotion_group, $form, $form_state);

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $promotion_group->status,
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the promotion_group entity form.
 */
function promotion_group_form_submit($form, &$form_state) {
  $promotion_group = $form_state['promotion_group'];

  entity_form_submit_build_entity('promotion_group', $promotion_group, $form, $form_state);
  entity_save('promotion_group', $promotion_group);

  $uri = entity_uri('promotion_group', $promotion_group);
  $form_state['redirect'] = $uri['path'];

  drupal_set_message(t('Promotion group %title saved.', array(
    '%title' => entity_label('promotion_group', $promotion_group),
  )));
}

/**
 * Entity: promotion_group_type =====================================================
 */

/**
 * Entity form: promotion_group_type
 */
function promotion_group_type_form($form, &$form_state, $promotion_group_type, $op = 'edit') {
  if ($op == 'clone') {
    $promotion_group_type->label .= ' (cloned)';
    $promotion_group_type->type = '';
  }

  $form_state['promotion_group_type'] = $promotion_group_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($promotion_group_type->label) ? $promotion_group_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($promotion_group_type->type) ? $promotion_group_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $promotion_group_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_promotion_groups_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($promotion_group_type->description) ? $promotion_group_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the promotion_group_type entity form.
 */
function promotion_group_type_form_submit(&$form, &$form_state) {
  $promotion_group_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('promotion_group_type', $promotion_group_type);

  // Redirect the user back to the list of promotion_group_type entities.
  $form_state['redirect'] = 'admin/structure/promotion-types/promotion-group-types';
}
