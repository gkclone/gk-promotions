<?php

/**
 * @file
 * Entity classes for the promotion_group(_type) entities.
 */

/**
 * Entity class: promotion_group
 */
class PromotionGroup extends Entity {
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/promotion/groups/' . $this->identifier(),
    );
  }
}
