<?php

/**
 * @file
 * API functions for the promotion_group(_type) entities.
 */

/**
 * Entity: promotion_group ==========================================================
 */

/**
 * Entity access callback: promotion_group
 */
function promotion_group_access($op, $promotion_group, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  static $administer_promotions = array(
    'create' => 1, 'update' => 1, 'edit' => 1, 'delete' => 1,
  );
  if (isset($administer_promotions[$op])) {
    return user_access('administer promotion_group', $account);
  }

  if ($op == 'view') {
    return $promotion_group->status && user_access('access content', $account);
  }
}

/**
 * Entity view callback: promotion_group
 */
function promotion_group_view($promotion_group, $view_mode = 'default', $langcode = NULL, $page = NULL) {
  return entity_view('promotion_group', array($promotion_group), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: promotion_group
 */
function promotion_group_load($tgid, $reset = FALSE) {
  $promotion_groups = promotion_group_load_multiple(array($tgid), array(), $reset);
  return reset($promotion_groups);
}

/**
 * Entity load multiple callback: promotion_group
 */
function promotion_group_load_multiple($tgids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('promotion_group', $tgids, $conditions, $reset);
}

/**
 * Entity: promotion_group_type =====================================================
 */

/**
 * List of promotion_group_type entities.
 */
function gk_promotion_groups_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('promotion_group_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): promotion_group_type
 */
function promotion_group_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer promotion group types', $account);
}

/**
 * Entity load callback: promotion_group_type
 */
function promotion_group_type_load($promotion_group_type) {
  return gk_promotion_groups_type_list($promotion_group_type);
}
