<?php
/**
 * @file
 * gk_promotion_groups.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function gk_promotion_groups_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'promotion_group-promotion_group_type__default-field_hide_title'
  $field_instances['promotion_group-promotion_group_type__default-field_hide_title'] = array(
    'bundle' => 'promotion_group_type__default',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'promotion_group',
    'field_name' => 'field_hide_title',
    'label' => 'Hide title',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'promotion_group-promotion_group_type__default-field_promotion_group_format'
  $field_instances['promotion_group-promotion_group_type__default-field_promotion_group_format'] = array(
    'bundle' => 'promotion_group_type__default',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'promotion_group',
    'field_name' => 'field_promotion_group_format',
    'label' => 'Format',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'promotion_group-promotion_group_type__default-field_promotion_refs'
  $field_instances['promotion_group-promotion_group_type__default-field_promotion_refs'] = array(
    'bundle' => 'promotion_group_type__default',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 0,
          'view_mode' => 'full',
        ),
        'type' => 'entityreference_entity_view',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'promotion_group',
    'field_name' => 'field_promotion_refs',
    'label' => 'Promotions',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Format');
  t('Hide title');
  t('Promotions');

  return $field_instances;
}
