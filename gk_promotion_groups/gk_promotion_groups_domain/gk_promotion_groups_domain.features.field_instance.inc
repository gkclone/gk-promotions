<?php
/**
 * @file
 * gk_promotion_groups_domain.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function gk_promotion_groups_domain_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'promotion_group-promotion_group_type__default-domain_e0199b0b3ece4201cb9f60a64'
  $field_instances['promotion_group-promotion_group_type__default-domain_e0199b0b3ece4201cb9f60a64'] = array(
    'bundle' => 'promotion_group_type__default',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'promotion_group',
    'field_name' => 'domain_e0199b0b3ece4201cb9f60a64',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Domain');

  return $field_instances;
}
