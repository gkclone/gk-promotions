<?php
/**
 * @file
 * gk_promotion_groups_domain.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function gk_promotion_groups_domain_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'domain_e0199b0b3ece4201cb9f60a64'
  $field_bases['domain_e0199b0b3ece4201cb9f60a64'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'promotion_group',
    ),
    'field_name' => 'domain_e0199b0b3ece4201cb9f60a64',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  return $field_bases;
}
